import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Action, Store } from '@ngrx/store';

import { postModel } from 'src/app/common/model/post.model';
import { FakeServiceService } from 'src/app/common/services/fake-service.service';
interface AppState {
  count: number
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  postList: Array<postModel>;
  counter: number;
  myForm: FormGroup;
  hasError: boolean = false;  

  constructor(private fakeServices: FakeServiceService, private store: Store<AppState>, private fb: FormBuilder) {
    this.postList = []
    this.store.subscribe(state => {
      this.counter = state.count
    })
  }

  ngOnInit(): void {
    this.getPost();
  }

  buildForm() {
    this.myForm = this.fb.group({
      postId: [this.postList.length + 1, ],
      id: [this.postList.length + 1, ],
      name: ['', [Validators.required]],
      email: ['',  [Validators.required, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')]],
      body: ['', [Validators.required]],
    })
  }


  getPost() {
    return this.fakeServices.getPosts().subscribe((response: postModel[]) => {
      if (response)
        this.formPostsData(response)
    }, error => {
      if (error)
      this.hasError = true;
    })
  }

  formPostsData(post: any) {
    this.postList = post
    this.dispatchActions(this.postList.length);
    this.buildForm()
  }

  addComment() {
    const comment: postModel = {
      body: this.myForm.get('body')?.value,
      email: this.myForm.get('email')?.value,
      id: this.postList.length + 1,
      name: this.myForm.get('name')?.value,
      postId: this.postList.length + 1
    }

    this.postList.push(comment);
    this.dispatchActions(this.postList.length );
  }

  dispatchActions(counter:number){
    const action: Action = {
      type: (counter).toString()
    }
    this.store.dispatch(action)
  }
  deleteComment  (id:number) {
    console.log(id);
    
      this.postList.splice(id, 1)
      this.dispatchActions(this.postList.length );
  }
  
}

