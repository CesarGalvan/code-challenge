export interface postModel{
    "postId": number;
    "id": number;
    "name": string;
    "email": string;
    "body": string;
}