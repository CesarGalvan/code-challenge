import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { postModel } from '../model/post.model';
@Injectable({
  providedIn: 'root'
})
export class FakeServiceService {

  URL: string = 'https://jsonplaceholder.typicode.com/posts/1/comments'
  constructor(private http: HttpClient) {
  }

  getPosts(): Observable<any> {
    return this.http.get(this.URL).pipe(
      catchError(err => {
        console.log(err);
        return of('Service Error for get Post');
      })
    );
  }
}
